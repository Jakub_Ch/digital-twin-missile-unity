using com.ggames4u.detonator_v3;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityStandardAssets.Water;
using static Gaia.GaiaConstants;

public class missile_seeker : MonoBehaviour
{
    [SerializeField] private float manual_rot_speed;
    [SerializeField] private GameObject target;//target object for location
    [SerializeField] private float azimuth;
    [SerializeField] private float elevation;

    private Rigidbody rb;//rigidbody info
    private GameObject camera;//camera for location
    private bool automatic_mode;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();//getting rigidbody
        target = GameObject.Find("Target");//getting target
        camera = GameObject.Find("Camera_Location");//getting camera_location empty object

        if (manual_rot_speed == 0)//if rotation speed is undefined set it to a value
        {
            manual_rot_speed = 0.25f;
        }

        if (elevation == 0 & azimuth == 0)//if we're not getting elevation and azimuth make the mode automatic
        {
            automatic_mode = true;
        }
    }

    // Update is called once per frame
    private void Update()
    {

    }

    // FixedUpdate is called once every physics update
    private void FixedUpdate()
    {
        rb.AddForce(transform.forward * -40f, ForceMode.Acceleration);//getting the missile to move (the rocket model is upside down so force is technically pushing it backwards)   

        Vector3 currentPosition = camera.transform.position;
        Vector3 targetPosition = target.transform.position;

        float missile_azimuth = transform.eulerAngles.y;//getting the direction the missile is pointing in
        float missile_elevation = transform.eulerAngles.x;
        float missile_roll = transform.eulerAngles.z;

        if (automatic_mode)
        {
            Vector3 right_rotation = Quaternion.LookRotation(currentPosition - targetPosition).eulerAngles;//Getting the rotation the missile needs to be to point at the target
            right_rotation.z = missile_roll;//Giving the rotation instructions the same roll as the missile since that won't effect where it's pointing
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(right_rotation), manual_rot_speed * 3);//rotating it towards that smoothly
        }
        else
        {
            float el_lim;//initialising limited elevation and azimuth
            float azi_lim;
            el_lim = Mathf.Min(manual_rot_speed, Mathf.Max(-manual_rot_speed, elevation));//limiting the value to be between -rot_speed and rot_speed
            azi_lim = Mathf.Min(manual_rot_speed, Mathf.Max(-manual_rot_speed, azimuth));
            transform.Rotate(-el_lim, -azi_lim, 0);//rotating the missile by that much
        }
      
        if (rb.velocity.magnitude > 0)//dampening the rotation based on it's velocity vector (this code makes the missile rotate to be in line with the velocity vector)
        {
            Vector3 target_rotation = Quaternion.LookRotation(rb.velocity).eulerAngles;//Create a rotation vector from velocity vector
            target_rotation.z = missile_roll;//make sure it doesn't change its roll
            float rotation_speed = Vector3.Angle(-transform.forward, rb.velocity);//see how big the change should be
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(-target_rotation), rotation_speed * 0.005f);//rotate it at that speed towards the velocity vecotr
        }
    }

}