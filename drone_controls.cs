using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class drone_controls : MonoBehaviour
{
    private Rigidbody rigidbody_component;
    private float horizontal_input;
    private float vertical_input;
    private float z_input;
    //private float mouse_fwd;
    //private float mouse_right;
    private bool shift_lock_out;
    private bool space_lock_out;
    private bool no_input;

    private float sqrt_three;
    private float sqrt_two;
    [SerializeField] private float speed;
    //private Vector3 rot;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody_component = GetComponent<Rigidbody>(); //Rigidbody variable
        sqrt_three = (float)Math.Sqrt(3);
        sqrt_two = (float)Math.Sqrt(2);
    }

    // Update is called once per frame
    void Update()
    {
        horizontal_input = Input.GetAxis("Horizontal"); //A and D (left arrow and right arrow)
        vertical_input = Input.GetAxis("Vertical"); //W and S (up arrow and down arrow)

        bool space = Input.GetKey(KeyCode.Space);//Initialising bools for the shift and space key interactions
        bool shift_down = Input.GetKeyDown(KeyCode.LeftShift);
        bool shift_up = Input.GetKeyUp(KeyCode.LeftShift);
        bool space_up = Input.GetKeyUp(KeyCode.Space);
        
        if (space & shift_down)//If shift is pressed while using space we lock out space. This way shift overrides space
        {
            space_lock_out = true;
        }
        if (shift_up | space_up)//Once we're back to only one key being pressed we can get rid of the override
        {
            space_lock_out = false;
        }

        bool shift = Input.GetKey(KeyCode.LeftShift);//More initialisations but for the shift lock out
        bool space_down = Input.GetKeyDown(KeyCode.Space);
        
        if (shift & space_down)//Same thing but with roles reversed
        {
            shift_lock_out = true;
        }
        if (space_up | shift_up)
        {
            shift_lock_out = false;
        }

        if (space & !space_lock_out)
        {
            z_input = 1;
        }else if(shift & !shift_lock_out)
        {
            z_input = -1;
        } else
        {
            z_input = 0;
        }

        if (horizontal_input * vertical_input * z_input != 0) //this is for when all three keys are being pressed in that case the values are preserved
        {
            
        }
        else if((horizontal_input + vertical_input) * (vertical_input + z_input) != 0) //this is for when two keys are being pressed
        {
            horizontal_input *= sqrt_two; 
            vertical_input *= sqrt_two;
            z_input *= sqrt_two;
        }
        else //this for when only one key is being pressed
        {
            horizontal_input *= sqrt_three;
            vertical_input *= sqrt_three;
            z_input *= sqrt_three;
        }

        if (horizontal_input * vertical_input * z_input == 0)
        {
            no_input = true;
        }
        // Mouse movements
        //mouse_fwd = Input.GetAxis("Mouse Y"); 
        //mouse_right = Input.GetAxis("Mouse X");
    }

    private void FixedUpdate()
    {
        if(speed == 0)
        {
            speed = 25;
        }
        rigidbody_component.AddForce(transform.TransformDirection(Vector3.right * horizontal_input * speed), ForceMode.Impulse);
        rigidbody_component.AddForce(transform.TransformDirection(Vector3.forward * vertical_input * speed), ForceMode.Impulse);
        rigidbody_component.AddForce(transform.TransformDirection(transform.up * z_input * speed), ForceMode.Impulse);

        if (no_input)
        {
            rigidbody_component.velocity = Vector3.zero;
        }

        //rot = Vector3.right * mouse_fwd * -1 + Vector3.up * mouse_right; //getting the euler angles from mouse movemetns

        //rigidbody_component.MoveRotation(Quaternion.Euler(rigidbody_component.rotation.eulerAngles + rot)); //adding them to the original rotations
        
    }
}